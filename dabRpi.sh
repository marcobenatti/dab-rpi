#!/bin/bash
sudo apt-get update
sudo apt-get install qt4-qmake build-essential g++
sudo apt-get install libsndfile1-dev qt4-default libfftw3-dev portaudio19-dev  libfaad-dev zlib1g-dev rtl-sdr libusb-1.0-0-dev mesa-common-dev libgl1-mesa-dev libqt4-opengl-dev libsamplerate-dev libqwt-dev
cd
 # http://www.sm5bsz.com/linuxdsp/hware/rtlsdr/rtlsdr.htm
wget http://sm5bsz.com/linuxdsp/hware/rtlsdr/rtl-sdr-linrad4.tbz
tar xvfj rtl-sdr-linrad4.tbz 
cd rtl-sdr-linrad4
sudo autoconf
sudo autoreconf -i
./configure --enable-driver-detach
make
sudo make install
sudo ldconfig
cd
git clone https://github.com/JvanKatwijk/dab-rpi.git
cp dab-rpi.pro dab-rpi/
cd dab-rpi
qmake dab-rpi.pro
make
cd 
git clone https://github.com/JvanKatwijk/sdr-j-dab.git
cp sdr-j-dab-0998.pro sdr-j-dab/
cd sdr-j-dab
qmake sdr-j-dab-0998.pro
make
cd
git clone https://github.com/JvanKatwijk/sdr-j-fm.git
cp sdr-j-fmreceiver-099.pro sdr-j-fm/
cd sdr-j-fm
qmake sdr-j-fmreceiver-099.pro
make

